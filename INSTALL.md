Install instructions for debian stretch.

``` bash
apt install git python3 python3-pip

echo "deb http://deb.debian.org/debian stretch-backports main contrib" >> /etc/apt/sources.list

apt update

cd /srv
git clone https://gitlab.com/iggdrasil/comm-on-net.git

cd comm-on-net
URL=commonnet.myorganisation.net RESPONSIBLE_EMAIL=dev@myorganisation.net ./install.sh
```
