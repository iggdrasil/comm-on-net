Modèle de données
=================

![Modèle de données](images/models.png "Modèle de données")

La partie métier du programme est découpée en deux modules :

- `commorganization` : regroupant les données concernant les structures
 interrogées ;
- `commcrawler` : regroupant les données concernant les sessions d'indexation.

commorganization
----------------

Ce module est centré sur le modèle `Organisation`. À celui-ci est rattaché un
`Type d'organisation` (exemple : Commune, Université, Média, etc.) et
éventuellement une `Zone`. La `Zone` est à entendre comme entité géographique,
les `Types de zone` reprennent de fait certains types d'organisation (Commune,
EPCI) mais ce découpage permet de regrouper les organisations qui partagent une
même zone géographique.

Chaque `Organisation` est associée à une ou plusieurs `Cibles`. Ces `Cibles`
sont les adresses Internet qui seront effectivement parcourues. Une même
`Organisation` peut donc avoir plusieurs adresses associées.

commcrawler
-----------

Ce module permet de stocker les éléments collectés lors des
`Sessions d'indexation`.
Une `Session d'indexation` est associées à plusieurs `Cibles`. Ainsi il est
possible de lancer des `Sessions d'indexation` en ne prenant en compte qu'un
sous-ensemble des `Cibles` définies. Un `Délai maximum` de parcours d'un site
peut être défini pour permettre notament d'identifier des branches de site
non pertinente et affiner le moteur (exemple : agenda).

À chaque `Session d'indexation` sont aussi associés des `Résultats d'indexation`.
Cette table reprend le décompte des éléments qui nous intéressent pour chaque
`Cible`. Le champ `Résultat d'indexation` collecte les listes d'éléments
pertinents (adresse des images, liens, etc.) permettant de faire a posteriori
le décompte.

En particulier la collecte exhaustive des liens externes rencontrés permet a
posteriori d'établir les éventuelles `Relations` entre les structures mais
permet aussi éventuellement d'identifier des nouvelles structures à intégrer
à l'étude.

La table `Domaine exclu` est à remplir avec les domaines "parking" et domaines
des hébergeurs pour identifier rapidement les sites aux domaines non
renouvellés (ils sont alors considérés comme hors ligne).
