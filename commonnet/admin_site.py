from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy as _


class CommOnNetAdminSite(AdminSite):
    site_header = _('Comm-on-net administration')
    site_title = _("Comm-on-net administration")


admin_site = CommOnNetAdminSite(name='commonnetadmin')

