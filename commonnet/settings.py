import os

from .scrapy_setting import SCRAPPY_SETTINGS
from .version import VERSION

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!lh+r$hzd(_-aj8a2&@)34bat=w&=!k+9w%$_+&^gjhf#n6z42'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# Cache for http request
DEV = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'commorganization.apps.CommOrganizationConfig',
    'commcrawler.apps.CommCrawlerConfig',
    'ajax_select',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'commonnet.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'commonnet.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'commonnet',
        'USER': 'commonnet',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'
USE_I18N = True
USE_L10N = True
USE_TZ = True

DATA_UPLOAD_MAX_NUMBER_FIELDS = 5000

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "collected_static")

NUMBER_PER_SESSION = 50

try:
    from .local_settings import *
except ImportError:
    print("WARNING: a local_settings.py must be defined")
    RESPONSIBLE_EMAIL = None
    EXTRA_APPS = []

if not RESPONSIBLE_EMAIL:
    print("ERROR: a RESPONSIBLE_EMAIL must be defined in your "
          "local_settings.py")
    exit(1)

SCRAPPY_SETTINGS["USER_AGENT"] = "commonnet-scraper v{} ({})".format(
    VERSION, RESPONSIBLE_EMAIL
)

if DEV:
    SCRAPPY_SETTINGS["HTTPCACHE_ENABLED"] = True

INSTALLED_APPS += EXTRA_APPS
