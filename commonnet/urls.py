from django.conf.urls import include, url

from ajax_select import urls as ajax_select_urls
from commonnet.admin_site import admin_site


urlpatterns = [
    url(r'^admin/', admin_site.urls),
    url(r'^ajax-select/', include(ajax_select_urls)),
]
