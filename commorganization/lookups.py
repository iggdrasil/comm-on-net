from ajax_select import register, LookupChannel as BaseLookupChannel

from django.db.models import Q

from . import models


class LookupChannel(BaseLookupChannel):
    def get_objects(self, items):
        # TODO: why IDs are not given here? M2M issue
        ids = []
        for item in items:
            if hasattr(item, 'pk'):
                ids.append(item.pk)
            else:
                ids.append(item)
        return super(LookupChannel, self).get_objects(ids)

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % str(item)


@register('organization')
class OrganizationLookup(LookupChannel):
    model = models.Organization

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = Q(name__icontains=term)
            query &= subquery
        return self.model.objects.filter(query).order_by('name')[:20]


@register('area')
class AreaLookup(LookupChannel):
    model = models.Area

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                    Q(name__icontains=term) |
                    Q(parent__name__icontains=term) |
                    Q(reference=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by('name')[:20]

