from io import StringIO

from django.core.management import call_command
from django.test import TestCase

from . import models


class CommandsTestCase(TestCase):
    def test_import_commune(self):
        out = StringIO()
        nb_area_type = models.AreaType.objects.count()
        nb_area = models.Area.objects.count()
        nb_org_type = models.OrganizationType.objects.count()
        nb_org = models.Organization.objects.count()
        nb_target = models.Target.objects.count()
        call_command('import_csv_communes',
                     'commorganization/tests/communes.csv', '--quiet',
                     stdout=out)
        nb_area_type_expected = nb_area_type + 4
        nb_area_expected = nb_area + 10
        nb_org_type_expected = nb_org_type + 4
        nb_org_expected = nb_org + 7
        nb_target_expected = nb_target + 7
        self.assertEqual(nb_area_type_expected, models.AreaType.objects.count())
        self.assertEqual(nb_area_expected, models.Area.objects.count())
        self.assertEqual(nb_org_type_expected,
                         models.OrganizationType.objects.count())
        self.assertEqual(nb_org_expected, models.Organization.objects.count())
        self.assertEqual(nb_target_expected, models.Target.objects.count())

    def test_import_autre(self):
        out = StringIO()
        nb_area_type = models.AreaType.objects.count()
        nb_area = models.Area.objects.count()
        nb_org_type = models.OrganizationType.objects.count()
        nb_org = models.Organization.objects.count()
        nb_target = models.Target.objects.count()
        call_command('import_csv_autres',
                     'commorganization/tests/autres.csv', '--quiet',
                     stdout=out)
        nb_area_type_expected = nb_area_type + 0
        nb_area_expected = nb_area + 0
        nb_org_type_expected = nb_org_type + 10
        nb_org_expected = nb_org + 13
        nb_target_expected = nb_target + 14
        self.assertEqual(nb_area_type_expected, models.AreaType.objects.count())
        self.assertEqual(nb_area_expected, models.Area.objects.count())
        self.assertEqual(nb_org_type_expected,
                         models.OrganizationType.objects.count())
        self.assertEqual(nb_org_expected, models.Organization.objects.count())
        self.assertEqual(nb_target_expected, models.Target.objects.count())
