from ajax_select import make_ajax_form
from django.contrib import admin, messages
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group, User
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from commonnet.admin_site import admin_site
from commorganization import models
from commcrawler.models import Crawl


admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)


class AreaTypeAdmin(admin.ModelAdmin):
    model = models.AreaType
    list_display = ('name', )


admin_site.register(models.AreaType, AreaTypeAdmin)


class AreaAdmin(admin.ModelAdmin):
    model = models.Area
    list_display = ('name', 'area_type', 'reference', 'parent')
    list_filter = ('area_type',)
    search_fields = ['name', 'parent__name']


admin_site.register(models.Area, AreaAdmin)


class OrganizationTypeAdmin(admin.ModelAdmin):
    model = models.OrganizationType
    list_display = ('name', 'parent')
    list_filter = ('parent',)


admin_site.register(models.OrganizationType, OrganizationTypeAdmin)


class OrganizationAdmin(admin.ModelAdmin):
    model = models.Organization
    list_display = ('name', 'organization_type', 'area')
    list_filter = ('organization_type',)
    search_fields = ['name']
    form = make_ajax_form(model, {'area': 'area'})


admin_site.register(models.Organization, OrganizationAdmin)


class AddToCrawlForm(forms.Form):
    crawl = forms.ChoiceField(label=_("Crawl"), choices=tuple())

    def __init__(self, *args, **kwargs):
        super(AddToCrawlForm, self).__init__(*args, **kwargs)
        self.fields["crawl"].choices = [(None, "--")] + [
            (c.pk, str(c))
            for c in Crawl.objects.filter(status="C").all()
        ]


class TargetAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'organization')
    model = models.Target
    list_filter = ('organization__organization_type',)
    form = make_ajax_form(model, {'organization': 'organization'})
    actions = ['add_to_crawl']
    search_fields = ["name", "organization__name"]

    def add_to_crawl(self, request, queryset):
        if 'apply' in request.POST:
            form = AddToCrawlForm(request.POST)
            if form.is_valid():
                crawl = None
                try:
                    crawl = Crawl.objects.get(
                        pk=form.cleaned_data["crawl"],
                        status="C"
                    )
                except models.Crawl.DoesNotExist:
                    pass
                if crawl:
                    nb_added = 0
                    targets = [
                        c["id"] for c in crawl.targets.values("id")
                    ]
                    for target in queryset.all():
                        if target.pk not in targets:
                            crawl.targets.add(target)
                            nb_added += 1
                    messages.add_message(
                        request, messages.INFO,
                        str(_("{} new targets added to {}")).format(
                            nb_added, str(crawl)
                        )
                    )
                    return HttpResponseRedirect(request.get_full_path())
        else:
            form = AddToCrawlForm()
        return render(
            request, 'admin/add_to_crawl.html',
            context={"form": form, "query_nb": queryset.count(),
                     "items": [v['pk'] for v in queryset.values('pk').all()]})

    add_to_crawl.short_description = _("Add to crawl")


admin_site.register(models.Target, TargetAdmin)
