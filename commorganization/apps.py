from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CommOrganizationConfig(AppConfig):
    name = 'commorganization'
    verbose_name = _("Organization")
