from django.db import models
from django.utils.translation import ugettext_lazy as _


class AreaType(models.Model):
    name = models.CharField(_("Name"), max_length=2000)

    class Meta:
        verbose_name = _("Area type")
        verbose_name_plural = _("Area types")
        ordering = ("name", )

    def __str__(self):
        return self.name


class Area(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=2000)
    area_type = models.ForeignKey(
        AreaType, on_delete=models.CASCADE, verbose_name=_("Type"))
    population = models.IntegerField(verbose_name=_("Population"), blank=True,
                                     null=True)
    reference = models.CharField(
        verbose_name=_("Reference"), max_length=100, blank=True, null=True,
        help_text=_("For instance, INSEE code for towns")
    )
    parent = models.ForeignKey("Area", verbose_name=_("Parent"),
                               blank=True, null=True)

    class Meta:
        verbose_name = _("Area")
        verbose_name_plural = _("Areas")
        ordering = ("name", )

    def __str__(self):
        if not self.parent:
            return self.name
        return "{} / {}".format(self.parent, self.name)


class OrganizationType(models.Model):
    name = models.CharField(_("Name"), max_length=2000)
    parent = models.ForeignKey("OrganizationType", verbose_name=_("Parent"),
                               blank=True, null=True)

    class Meta:
        verbose_name = _("Organization type")
        verbose_name_plural = _("Organization types")
        ordering = ("parent__name", "name", )

    def __str__(self):
        if not self.parent:
            return self.name
        return "{} / {}".format(self.parent, self.name)


class Organization(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=2000)
    area = models.ForeignKey(
        Area, on_delete=models.SET_NULL, verbose_name=_("Area"), blank=True,
        null=True
    )
    organization_type = models.ForeignKey(
        OrganizationType, on_delete=models.CASCADE, verbose_name=_("Type"))
    address = models.TextField(verbose_name=_("Address"), blank=True, null=True)

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")
        ordering = ("name", )

    def __str__(self):
        if not self.area:
            return "{} ({})".format(
                self.name, self.organization_type)
        return "{} - {} ({})".format(
            self.name, self.area, self.organization_type)


class Target(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=200)
    url = models.URLField(verbose_name=_("URL"), blank=True, null=True)
    organization = models.ForeignKey(
        Organization, verbose_name=_("Organization"), blank=True,
        null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Target")
        verbose_name_plural = _("Targets")
        ordering = ("name",)

    def __str__(self):
        return "{} ({})".format(self.name, self.organization)
