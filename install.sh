#!/bin/bash

DEBUG=false  # true

set -e
[ "$DEBUG" == 'true' ] && set -x

if [ -z "$URL" ]; then echo "Error: URL env variable must be set. Exiting..."; exit 1; fi
if [ -z "$RESPONSIBLE_EMAIL" ];
  then echo "Error: RESPONSIBLE_EMAIL env variable must be set. Exiting..."; exit 1; fi

echo " * Install dependencies"

APT_OPTIONS=" -y -q "

[ "$DEBUG" == 'true' ] && APT_OPTIONS=""

apt-get install $APT_OPTIONS git nginx uwsgi uwsgi-plugin-python3 postgresql apg sed gettext > /dev/null
apt-get install $APT_OPTIONS python3 python3-pip python3-psycopg2 python3-pygments python3-service-identity > /dev/null
apt-get install $APT_OPTIONS -t stretch-backports python3-django > /dev/null
# buster/bulleyes: apt install python3-django python3-requests

pip3 install scrapy==1.7 tldextract==2.2 django-ajax-selects==1.6.0 service_identity==18.1 requests[security] > /dev/null
# buster: apt install python3-tldextract django-ajax-selects
# buster: apt install -t buster-backports python3-service-identity
# bullseye: apt install python3-scrapy python3-tldextract django-ajax-selects python3-service-identity

PASSWORD=`apg -n 1 -a 1 -m 24 -M CLN`
PWD=`pwd`
DB_NAME=commonnet

echo " * Creating conf files"

mkdir -p /var/log/django/
mkdir -p /var/log/scrapy/
mkdir -p /var/lib/uwsgi/run
chown -R www-data:www-data /var/lib/uwsgi/run
chown -R www-data:www-data /var/log/django/
cp conf/scrapy.logrotate /etc/logrotate.d/scrapy

sed -s "s|#URL#|$URL|g;s|#PASSWORD#|$PASSWORD|g;\
        s|RESPONSIBLE_EMAIL = None|RESPONSIBLE_EMAIL = '$RESPONSIBLE_EMAIL'|;" \
       commonnet/local_settings.py.sample > conf/local_settings.py
rm -f commonnet/local_settings.py
ln -s "$PWD"/conf/local_settings.py commonnet/

sed -s "s|#URL#|$URL|g;" conf/uwsgi.ini.template > \
    conf/uwsgi.ini
rm -f /etc/uwsgi/apps-enabled/commonnet.ini
ln -s "$PWD"/conf/uwsgi.ini /etc/uwsgi/apps-enabled/commonnet.ini

sed -s "s|#URL#|$URL|g;" conf/nginx.conf.template > \
    conf/nginx.conf
rm -f /etc/nginx/sites-enabled/commonnet.conf
ln -s "$PWD"/conf/nginx.conf /etc/nginx/sites-enabled/commonnet.conf

echo " * Creating database $DB_NAME"
if ! su postgres -c "psql -l" | grep -qs "$DB_NAME"; then
  su postgres -c "createuser --echo --adduser --createdb --encrypted $DB_NAME"
  echo "ALTER USER \""$DB_NAME"\" with password '"$PASSWORD"';" > /tmp/inst.sql
  su postgres -c  "psql -f /tmp/inst.sql"
  rm /tmp/inst.sql
  su postgres -c "createdb --echo --owner $DB_NAME --encoding UNICODE $DB_NAME"
else
  echo "Error: commonnet database already present. Exiting..."
  exit 1
fi

make update

echo " * create super user"
python3 manage.py createsuperuser

systemctl restart uwsgi nginx
