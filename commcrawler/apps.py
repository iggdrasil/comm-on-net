from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CommCrawlerConfig(AppConfig):
    name = 'commcrawler'
    verbose_name = _("Crawler")
