from ajax_select import register

from django.db.models import Q

from commorganization.lookups import LookupChannel

from . import models


@register('target')
class TargetLookup(LookupChannel):
    model = models.Target

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = Q(name__icontains=term) | Q(url__icontains=term)
            query &= subquery
        return self.model.objects.filter(query).order_by('name')[:20]

