scrapy==1.7
tldextract==2.2
django==1.11
django-ajax-selects==1.6.0
psycopg2
pygments==2.2
requests==2.21
service_identity==18.1
# https://splash.readthedocs.io/